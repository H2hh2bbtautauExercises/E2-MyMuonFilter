#include "H2hh2bbtautauExercises/E2-MyMuonFilter/interface/MyGenericObjectSelectorFilter.h"
#include "H2hh2bbtautauExercises/E2-MyMuonFilter/interface/MyPATMuonSelector.h"
#include "FWCore/Framework/interface/MakerMacros.h"

typedef MyGenericObjectSelectorFilter<pat::MuonCollection,MyPATMuonSelector,pat::MuonCollection> MyPATMuonCloneSelectorFilter;
DEFINE_FWK_MODULE( MyPATMuonCloneSelectorFilter );


