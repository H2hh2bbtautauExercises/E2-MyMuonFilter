#! /usr/bin/env python

import ROOT
import sys
from DataFormats.FWLite import Events, Handle

# Make VarParsing object
# https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideAboutPythonConfigFile#VarParsing_Example
from FWCore.ParameterSet.VarParsing import VarParsing
options = VarParsing ('python')
options.parseArguments()

# use Varparsing object
events = Events (options)

# create handle outside of loop
handle_allmuons  = Handle ("std::vector<pat::Muon>")
handle_selmuons  = Handle ("std::vector<pat::Muon>")

# Create histograms, etc.
ROOT.gROOT.SetBatch()        # don't pop up canvases
ROOT.gROOT.SetStyle('Plain') # white background
ptAllMuons = ROOT.TH1F ("allmuonpt", "Muon pt", 50, 0, 100)
ptSelMuons = ROOT.TH1F ("selmuonpt", "Muon pt", 50, 0, 100)

# loop over events
for event in events:
    event.getByLabel ("slimmedMuons", handle_allmuons)
    event.getByLabel ("selectedMuons", handle_selmuons)
    allmuons = handle_allmuons.product()
    selmuons = handle_selmuons.product()

    for muon in allmuons:
        ptAllMuons.Fill(muon.pt())
    
    for muon in selmuons:
        ptSelMuons.Fill(muon.pt())

# make a canvas, draw, and save it
c1 = ROOT.TCanvas()
c1.SetLogy()
ptAllMuons.SetLineColor(ROOT.kRed)
ptSelMuons.SetLineColor(ROOT.kGreen)
ptSelMuons.SetLineWidth(2)
ptAllMuons.Draw()
ptSelMuons.Draw("same")
c1.Print ("muon_pt.png")
