import FWCore.ParameterSet.Config as cms

process = cms.Process("EXERCISE")

process.load("FWCore.MessageService.MessageLogger_cfi")

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

process.options  = cms.untracked.PSet( wantSummary = cms.untracked.bool(True))


process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
        'file:/pnfs/desy.de/cms/tier2/store/mc/RunIISpring16MiniAODv2/GluGluToRadionToHHTo2B2Tau_M-300_narrow_13TeV-madgraph/MINIAODSIM/PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14_ext1-v1/80000/02EE1330-5D3B-E611-B5AD-001CC4A7C0A4.root'
    )
)

process.myMuonCollectionWithIsolation = cms.EDProducer('MyMuonIsolationProducer',
    muons = cms.InputTag('slimmedMuons')
)

process.selectedMuons = cms.EDFilter("MyPATMuonCloneSelectorFilter",
                                     src = cms.InputTag('myMuonCollectionWithIsolation'),
                                     vertices = cms.InputTag('offlineSlimmedPrimaryVertices'),
                                     ptmin = cms.double(20),
                                     etamax = cms.double(2.1),   
                                     vtxdxymax = cms.double(0.045),                 
                                     vtxdzmax = cms.double(0.2),
                                     takeMuonID = cms.string("medium"),
                                     iso = cms.double(0.15),
                                     filter = cms.bool(True)
                                     )

process.out = cms.OutputModule("PoolOutputModule",
    fileName = cms.untracked.string('mySelectedMuonOutputFile.root'),
    outputCommands = cms.untracked.vstring(
        'drop *',
        'keep *_slimmedMuons_*_*',
        'keep *_*_*_EXERCISE'
        ),
    SelectEvents = cms.untracked.PSet(SelectEvents = cms.vstring("p"))
)
  
process.p = cms.Path(process.myMuonCollectionWithIsolation*process.selectedMuons)

process.e = cms.EndPath(process.out)
