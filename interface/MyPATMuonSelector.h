#include <vector>
#include <string>

#include "FWCore/Framework/interface/ConsumesCollector.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "FWCore/Utilities/interface/EDGetToken.h"

#include "DataFormats/VertexReco/interface/VertexFwd.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/PatCandidates/interface/Muon.h"


namespace edm {
  class ParameterSetDescription;
}

namespace {
  class MyPATMuonSelector {
  public:
    MyPATMuonSelector(edm::ParameterSet const& pset, edm::ConsumesCollector& cc);
    void preChoose(edm::Event const& event, edm::EventSetup const& es);
    bool choose(unsigned int iIndex, pat::Muon const& iItem);

  private:
    edm::EDGetTokenT<reco::VertexCollection> vtxToken_;
    edm::Handle<reco::VertexCollection> vertices;

    double ptmin_;
    double etamax_;
    double vtxdxymax_;
    double vtxdzmax_;

    double iso_;

    std::string takeMuonID_;
  };
  
  MyPATMuonSelector::MyPATMuonSelector(edm::ParameterSet const& pset, edm::ConsumesCollector& cc){
    // Get the values of parameters that you need
    vtxToken_ = cc.consumes<reco::VertexCollection>(pset.getParameter<edm::InputTag>("vertices"));
    ptmin_ = pset.getParameter<double>("ptmin");
    etamax_ = pset.getParameter<double>("etamax");
    vtxdxymax_ = pset.getParameter<double>("vtxdxymax");
    vtxdzmax_ = pset.getParameter<double>("vtxdzmax");
    takeMuonID_ = pset.getParameter<std::string>("takeMuonID");
    iso_ = pset.getParameter<double>("iso");
  }

  void MyPATMuonSelector::preChoose(edm::Event const& event, edm::EventSetup const& es) {
    //Get VertexCollection
    event.getByToken(vtxToken_, vertices);
  }

  bool MyPATMuonSelector::choose( unsigned int iIndex, pat::Muon const& muon) {
    // Return true if you select a particular element of the master collection
    // and want it saved in the thinned collection. Otherwise return false.
    bool muon_iso = false;
    bool muon_id = false;
    bool muon_pv = false;
    bool muon_acc = false;

    const reco::Vertex &PV = vertices->front();

    //Muon ID "loose","medium" and "tight"
    if(takeMuonID_=="medium" && muon.isMediumMuon()) 
      muon_id = true;
    if(takeMuonID_=="loose" && muon.isLooseMuon()) 
      muon_id = true;
    if(takeMuonID_=="tight" && muon.isTightMuon(PV))
      muon_id = true; 
    if(takeMuonID_!="loose" && takeMuonID_!="medium" && takeMuonID_!="tight")
      std::cout << "Muon ID has to be 'loose', 'medium', or 'tight'" << std::endl;

    //Muon PV
    if(fabs(muon.muonBestTrack()->dxy(PV.position())) < vtxdxymax_ && fabs(muon.muonBestTrack()->dz(PV.position()))  < vtxdzmax_) 
      muon_pv = true; 

    //Muon acceptance
    if(muon.pt() > ptmin_ && fabs(muon.eta()) < etamax_)
      muon_acc = true;

    //Muon isolation
    double isovalue = muon.userIsolation(pat::IsolationKeys(7));
    if(isovalue < iso_)
      muon_iso=true;
    if(isovalue < 0){
      std::cout << "muon isolation not set correctly!" << std::endl;
      muon_iso=false;
    }
    
    return(muon_id && muon_pv && muon_acc && muon_iso);
  }
}
